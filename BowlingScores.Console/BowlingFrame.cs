﻿using System.Runtime.Remoting.Messaging;

namespace BowlingScores.Console
{
    public class BowlingFrame
    {
        public int FirstThrow { get; set; }

        public int SecondThrow { get; set; }

        public bool IsSpare => (FirstThrow + SecondThrow == 10) && !IsStrike;

        public bool IsStrike => FirstThrow == 10;
    }
}

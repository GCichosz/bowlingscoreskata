﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingScores.Console
{
    public class BowlingMatch
    {
        public int GetScore(int[][] inputs)
        {
            var frames = inputs.Select(ArrayToFrame).ToArray();

            int score = 0;
            for (var i = 0; i < frames.Length - 1; i++)
            {
                var currentFrame = frames[i];

                var currentFrameScore = currentFrame.FirstThrow + currentFrame.SecondThrow;

                if (currentFrame.IsSpare || currentFrame.IsStrike)
                {
                    score += frames[i + 1].FirstThrow;

                    if (currentFrame.IsStrike)
                    {
                        if (frames[i+1].IsStrike)
                        {
                            score += frames[i + 2].FirstThrow;
                        }
                        else
                        {
                            score += frames[i + 1].SecondThrow;
                        }
                    }
                }
                score += currentFrameScore;
            }
            return score;
        }

        private BowlingFrame ArrayToFrame(int[] input)
        {
            return new BowlingFrame()
            {
                FirstThrow = input[0],
                SecondThrow = input[1]
            };
        }
    }
}

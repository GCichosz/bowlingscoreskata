﻿using BowlingScores.Console;
using NUnit.Framework;

namespace BowlingScores.Test
{
    [TestFixture]
    public class BowlingFrameTests
    {
        [Test]
        public void FrameSouldBeMarkedSpareWhenSpare()
        {
            var frame = new BowlingFrame
            {
                FirstThrow = 4,
                SecondThrow = 6
            };

            Assert.IsTrue(frame.IsSpare);
            Assert.IsFalse(frame.IsStrike);
        }

        [Test]
        public void FrameShouldBeMarkedStrikeWhenStrike()
        {
            var frame = new BowlingFrame
            {
                FirstThrow = 10,
                SecondThrow = 0
            };

            Assert.IsTrue(frame.IsStrike);
            Assert.IsFalse(frame.IsSpare);
        }

        [Test]
        public void FrameSouldNotBeMarkedWhenNotSpareNotStrike()
        {
            var frame = new BowlingFrame
            {
                FirstThrow = 4,
                SecondThrow = 5
            };

            Assert.IsFalse(frame.IsSpare);
            Assert.IsFalse(frame.IsStrike);
        }
    }
}

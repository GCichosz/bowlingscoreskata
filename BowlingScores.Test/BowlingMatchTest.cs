﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BowlingScores.Console;
using NUnit.Framework;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace BowlingScores.Test
{
    [TestFixture]
    class BowlingMatchTest
    {
        private readonly BowlingMatch _bowlingMatch;

        public BowlingMatchTest()
        {
            _bowlingMatch = new BowlingMatch();
        }

        [Test]
        public void ShouldScore0WhenNoPinsKnocked()
        {
            int [][] inputs = new int[][] {            
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 }
            };

            int score = _bowlingMatch.GetScore(inputs);

            Assert.AreEqual(0, score);
        }

        [Test]
        public void ShouldSimplySumScoresWhenNoSpareAndNoStrike()
        {
            int[][] inputs = new int[][] {
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 1, 1 },
                new[] { 0, 0 }
            };

            int score = _bowlingMatch.GetScore(inputs);

            Assert.AreEqual(20, score);
        }

        [Test]
        public void ShouldReturn160WhenAllSparesWithFirstBallAlways6()
        {
            int[][] inputs = new int[][] {
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 4 },
                new[] { 6, 0 }
            };

            int score = _bowlingMatch.GetScore(inputs);

            Assert.AreEqual(160, score);
        }

        [Test]
        public void ShouldReturn240When9StrikesFollowedBy0()
        {
            int[][] inputs = new int[][] {
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 10, 0 },
                new[] { 0, 0 },
                new[] { 0, 0 }
            };

            int score = _bowlingMatch.GetScore(inputs);

            Assert.AreEqual(240, score);
        }
    }
}
